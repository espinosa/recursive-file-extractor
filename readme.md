Extract all matched files from directories, ZIP and JAR archives; all
searched recursively.

It was originally created to extract icon image files from Eclipse
installation, from all its subdirectories and plugin JARs. Since then it was
abstracted to extract any type of file content.

It contains executable class
`org.bitbucket.espinosa.extractor.run.ExtractIcons` 
extracting all png, svg and gif icon files from given Eclipse installation.
It can be easily adapted to do the same for NetBeans or IDEA IntelliJ.