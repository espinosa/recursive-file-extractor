package org.bitbucket.espinosa.extractor;

import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Convenience wrapper from handling archive files using Java 7 NIO {@link FileSystem}.
 * 
 * TODO: move to external project, micro library?
 * 
 * @author Espinosa
 */
public class ArchiveHandler {
	
	/**
	 * Static constructor method
	 * @param archiveFile
	 * @return
	 */
	public static ArchiveHandlerImpl get(Path archiveFile) {
		return new ArchiveHandlerImpl(archiveFile);
	}

	public static class ArchiveHandlerImpl {
		private final Path archiveFile;

		private ArchiveHandlerImpl(Path archiveFile) {
			this.archiveFile = archiveFile;
		}
		
		/**
		 * List all files and directories, prefiltered with predicate.
		 * 
		 * @param predicate example: {@code Files::isRegularFile} to extract only file references
		 * 
		 * @return
		 */
		public List<Path> listContentFromArchive(Predicate<Path> predicate) throws Exception {
			List<Path> result = new LinkedList<>();
			try (FileSystem fs = FileSystems.newFileSystem(archiveFile, null)) {
				for (Path root : fs.getRootDirectories()) {
					List<Path> r = Files.walk(root)
							.filter(predicate)
							.collect(Collectors.toList());
					result.addAll(r);
				}
			}
			return result;
		}
		
		public void walkArchive(Consumer<Stream<Path>> walkAction) throws IOException {
			try (FileSystem fs = FileSystems.newFileSystem(archiveFile, null)) {
				walkAction.accept(Files.walk(fs.getPath("/")));
			}
		}

		public Path extractFileFromArchive(String pathInArchive, Path destFolder) throws Exception {
			try (FileSystem fs = FileSystems.newFileSystem(archiveFile, null)) {
				Path fileInsideZipPath = fs.getPath(pathInArchive);
				Path destFile = destFolder.resolve(pathInArchive);
				if (destFile.getParent() != null) Files.createDirectories(destFile.getParent());
				Files.copy(fileInsideZipPath, destFile, StandardCopyOption.REPLACE_EXISTING);
				return destFile;
			}
		}

		public void addFileToArchive(String pathInArchive, Path srcFile) throws Exception {        
			try (FileSystem fs = FileSystems.newFileSystem(archiveFile, null)) {
				Path fileInsideZipPath = fs.getPath(pathInArchive);
				if (fileInsideZipPath.getParent() != null) Files.createDirectories(fileInsideZipPath.getParent());
				Files.copy(srcFile, fileInsideZipPath, StandardCopyOption.REPLACE_EXISTING);
			}
		}
	}
}
