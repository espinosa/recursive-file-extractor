package org.bitbucket.espinosa.extractor;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Extract all matched files from directories, ZIP and JAR archives; all
 * searched recursively.
 * 
 * It was originally created to extract icon image files from Eclipse
 * installation, from all its subdirectories and plugin JARs. Since then it was
 * abstracted to extract any type of file content.
 * 
 * @author Espinosa
 */
public class ExtractResources {
	private final Logger log = LoggerFactory.getLogger(ExtractResources.class);

	/**
	 * Map file name to extraction record. It is used to detect if same file
	 * name was already extracted, so we have a collision in name. 
	 * If content is unique, a new, unique filename is created for this file.
	 */
	private final Map<String, ExtractedFile> namesMap    = new HashMap<>();
	
	/**
	 * Map file content to extraction record. It is used to detect if same file
	 * was already extracted. Duplicities in files are just recorded, in
	 * locations, but not added to results again.
	 */
	private final Map<byte[], ExtractedFile> contentMap = new HashMap<>();

	private final UniqueNameHandler uniqueName = new UniqueNameHandler();
	private final Predicate<Path> relevantFileMatcher;
	
	private final Path sourceDir;
	private final Path destinationDir;
	

	/**
	 * Define resource extraction; from where, to where and what to extract.
	 * 
	 * @param sourceDir
	 *            defines extract from, root directory for the recursive search
	 * @param destinationDir
	 *            defines where extract to
	 * @param relevantFileMatcher
	 *            defines what files we are looking for
	 */
	public ExtractResources(String sourceDir, String destinationDir, Predicate<Path> relevantFileMatcher) {
		this.sourceDir = Paths.get(sourceDir);
		this.destinationDir = Paths.get(destinationDir);
		this.relevantFileMatcher = relevantFileMatcher;
	}

	/**
	 * Define resource extraction; from where, to where and what to extract.
	 * 
	 * @param sourceDir
	 *            defines extract from, root directory for the recursive search
	 * @param destinationDir
	 *            defines where extract to
	 * @param relevantFileMatcher
	 *            defines what files we are looking for
	 */
	public ExtractResources(Path sourceDir, Path destinationDir, Predicate<Path> relevantFileMatcher) {
		this.sourceDir = sourceDir;
		this.destinationDir =  destinationDir;
		this.relevantFileMatcher = relevantFileMatcher;
	}

	/**
	 * Run the extraction
	 * 
	 * @return operation report
	 */
	public Map<String, ExtractedFile> run() {
		try {
			Files.walk(sourceDir).forEach((p) -> {
				if (isRelevantFile(p)) {
					extractFile(p);
				} else if (isArchive(p)) {
					searchArchive(p);
				}
			});
			return namesMap;
		} catch (IOException e) {
			throw new ExtractorException(e);
		}
	}

	private boolean isRelevantFile(Path p) {
		return relevantFileMatcher.test(p);
	}

	private boolean isArchive(Path p) {
		if (p.getFileName() == null) return false;
		String n = p.getFileName().toString();
		return (n.endsWith(".jar") ||  n.endsWith(".zip"));
	}

	private void extractFile(Path path) {
		log.info("Extracting " + UriUtils.stripSchema(path.toUri()));
		try {
			String fileName = path.getFileName().toString();
			URI pathUri = path.toUri();
			byte[] contentHash = computeContentDigestHash(path);
			ExtractedFile existingNameRec = namesMap.get(fileName);
			ExtractedFile existingContentRec = contentMap.get(contentHash);
			ExtractedFile extractionRecord = null;

			// new content but name is taken
			// update name to be unique and register it
			if (existingNameRec != null && existingContentRec == null) {
				fileName = uniqueName.increment(fileName, namesMap);
			}

			// new content (target name is guranteed to be unique)
			if (existingContentRec == null) {
				Path fileDest = destinationDir.resolve(fileName);
				Files.copy(path, fileDest, StandardCopyOption.REPLACE_EXISTING);
				extractionRecord = new ExtractedFile(fileName, pathUri);
				writeNewAccFile(path, fileName);
			} 
			// existing content, do not copy file content, just register it for the record
			else {
				existingContentRec.getLocations().add(pathUri);
				extractionRecord = existingContentRec;
				appendToAccFile(path, fileName);
			}

			namesMap.put(fileName, extractionRecord);
			contentMap.put(contentHash, extractionRecord);
			
		} catch (IOException e) {
			throw new ExtractorException("Extraction failed on " + path, e);
		}
	}


	
	private void writeNewAccFile(Path fileSourcePath, String fileName) {
		writeAccFile(fileSourcePath, fileName, StandardOpenOption.WRITE);
	}

	private void appendToAccFile(Path fileSourcePath, String fileName) {
		writeAccFile(fileSourcePath, fileName, StandardOpenOption.APPEND);
	}
	
	/**
	 * Write to extracted file accompanying text file. It contains list of all source
	 * locations of that file (by content)
	 * 
	 * @param fileSourcePath
	 * @param fileName
	 * @param fileWriteOption
	 */
	private void writeAccFile(Path fileSourcePath, String fileName, OpenOption fileWriteOption) {
		String accFileName = fileName + ".txt";
		Path accFilePath = destinationDir.resolve(accFileName);
		try {
			String accFileContentToAppend = String.format("Origin: %s%n", UriUtils.stripSchema(fileSourcePath.toUri()));
			Files.write(accFilePath, accFileContentToAppend.getBytes(), fileWriteOption);
		} catch (IOException e) {
			throw new ExtractorException("Extraction failed for " 
					+ fileName + " from " + fileSourcePath 
					+ " on creation of acc file " + accFilePath, e);
		}
	}

	private byte[] computeContentDigestHash(Path file) {
		MessageDigest messageDigest;
		try {
			messageDigest = MessageDigest.getInstance("MD5");
			messageDigest.update(Files.readAllBytes(file));
		} catch (NoSuchAlgorithmException e) {
			throw new ExtractorException(e);
		} catch (IOException e) {
			throw new ExtractorException("Unable to compute has for " + file, e);
		}
		byte[] hashBytes = messageDigest.digest();
		return hashBytes;

	}

	/**
	 * Recursively search content of the given archive
	 */
	private void searchArchive(Path archivePath) {
		try {
			ArchiveHandler.get(archivePath).walkArchive((Stream<Path> stream) -> {
				stream.forEach((p) -> {
					if (isRelevantFile(p)) {
						extractFile(p);
					}});
				});
		} catch (IOException e) {
			throw new ExtractorException(e);
		}
	}
}
