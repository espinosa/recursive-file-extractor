package org.bitbucket.espinosa.extractor;

import java.net.URI;
import java.util.LinkedList;
import java.util.List;

public class ExtractedFile {

	/** extracted file name in the target directory (target name) */
	private final String name;

	/** JAR locations, usually only one but there can be more then one */
	private final List<URI> locations; 

	public ExtractedFile(String name, URI location) {
		this.name = name;
		this.locations = new LinkedList<>();
		locations.add(location);
	}

	public String getName() {
		return name;
	}

	public List<URI> getLocations() {
		return locations;
	}

	@Override
	public String toString() {
		return "ExtractedFile [name=" + name + ", locations=" + locations + "]";
	}
}