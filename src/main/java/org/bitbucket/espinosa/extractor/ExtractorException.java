package org.bitbucket.espinosa.extractor;

public class ExtractorException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public ExtractorException() {
		super();
	}

	public ExtractorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ExtractorException(String message, Throwable cause) {
		super(message, cause);
	}

	public ExtractorException(String message) {
		super(message);
	}

	public ExtractorException(Throwable cause) {
		super(cause);
	}
}
