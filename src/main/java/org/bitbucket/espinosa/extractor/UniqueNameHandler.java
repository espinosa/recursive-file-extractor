package org.bitbucket.espinosa.extractor;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Transform given name by adding or increasing suffix index like in this examples:
 * 
 * arrow.png      -> arrow.1.png
 * arrow.1.png    -> arrow.2.png
 * arrow.100.png  -> arrow.101.png
 * arrow.0.png    -> arrow.1.png
 * 
 * Repeat process until there is no collision in the namesMap.
 * 
 * @author Espinosa (6.4.2017)
 */
public class UniqueNameHandler {
	
	private final Pattern numberPattern = Pattern.compile("\\.(\\d*)\\.([^\\.]*)$");
	private final int initialIndex = 1;

	public String increment(String iconFileName, Map<String, ?> namesMap) {
		do {
			Matcher m = numberPattern.matcher(iconFileName);
			if (m.find()) {
				int index = Integer.valueOf(m.group(1));
				index++; 
				iconFileName = m.replaceFirst("." + String.valueOf(index) + ".$2" );
			} else {
				iconFileName = startIndexing(iconFileName);
			}
		} while(namesMap.get(iconFileName) != null);
		return iconFileName;
	}
	
	private String startIndexing(String s) {
		int p = s.lastIndexOf('.');
		return s.substring(0, p) + "." + Integer.valueOf(initialIndex) + s.substring(p, s.length());
	}
}
