package org.bitbucket.espinosa.extractor;

import java.net.URI;

/**
 * Overcome several shortcommings from stock {@link URI} implementation, notably
 * the way how directory and archives handled differently, notably to be able to
 * relativize their path towards same base directory.
 * 
 * @author Espinosa
 */
public class UriUtils {

	/**
	 * Overcome shortcommings from stock {@link URI#relativize(URI)} implementation in
	 * the way how directory and archive URIs cannot be relativized towards their path towards same base directory.
	 * 
	 * Example 1, file in a regular directory URI:
	 * {@code relativize("file:///C:/dev/workspace/proj123", "file:///C:/dev/workspace/proj123/target/test-classes/folder123/synced.png") => "folder123/synced.png"}
	 * 
	 * Example 2, file in a JAR URI:
	 * {@code relativize("file:///C:/dev/workspace/proj123", "jar:file:///C:/dev/workspace/proj123/target/test-classes/archive2.zip!/logo.png") => "archive2.zip!/logo.png"}
	 *
	 * @param base
	 * @param subBase
	 * @return
	 */
	public static String relativize(URI base, URI subBase) {
		// not yet implemented
		String base_ = stripSchema(base);
		String subBase_ = stripSchema(subBase);
		if (subBase_.startsWith(base_)) {
			return subBase_.substring(base_.length(), subBase_.length());
		} else {
			return subBase_;
		}
	}

	/**
	 * Strip schema from URI, schemas like "file:/" or "jar:file:/" 
	 * <p>
	 * Examples:<br>
	 * {@code file:///C:/dev/workspace/proj123/target/test-classes/folder123/synced.png             -> C:/dev/workspace/proj123/target/test-classes/folder123/synced.png (Windows)}<br>
	 * {@code file:///home/espinosa/dev/workspace/proj123/target/test-classes/folder123/synced.png  -> /home/espinosa/dev/workspace/proj123/target/test-classes/folder123/synced.png (Unix)}<br>
	 * {@code jar:file:///C:/dev/workspace/proj123/target/test-classes/archive2.zip!/logo.png       -> C:/dev/workspace/proj123/target/test-classes/archive2.zip!/logo.png}<br>
	 */
	public static String stripSchema(URI uri) {
		String uriAsStr = uri.toString();
		int p = uriAsStr.indexOf(":/");
		String uriStripped = uriAsStr.substring(p + 2, uriAsStr.length()); // example: "//C:/dev/synced.png"

		int slashPrefixLength = 0;
		while (uriStripped.charAt(slashPrefixLength) == '/') {
			slashPrefixLength++;
		};

		String uriSanitized;
		if (isWindowsStylePath(uriStripped, slashPrefixLength)) {
			// UNIX style, keep prefixing "/"
			uriSanitized = uriStripped.substring(slashPrefixLength - 1, uriStripped.length()); 
		} else {
			// WIN style path, no prefixing "/"
			uriSanitized = uriStripped.substring(slashPrefixLength, uriStripped.length()); 
		}

		return uriSanitized;
	}


	/**
	 * Try to detect windows paths like "C:/". It checks presence of ":/" on
	 * positions 1 and 2, optionally compensated by offset.
	 */
	private static boolean isWindowsStylePath(String s, int offset) {
		return (s.indexOf(":/") - offset) != 1;
	}
}
