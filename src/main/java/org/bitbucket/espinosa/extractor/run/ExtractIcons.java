package org.bitbucket.espinosa.extractor.run;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

import org.bitbucket.espinosa.extractor.ExtractResources;
import org.bitbucket.espinosa.extractor.ExtractedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Runnable tool.
 * Extract all icons from Eclipse installation.
 * Must be run from within IDE.
 * It get list of all plugin bundles, locate their JARs 
 * 
 * @author Espinosa
 */
public class ExtractIcons {
	public static final String ECLIPSE_HOME = "C:\\Users\\Espinosa\\.p2\\pool";
	public static final String DESTINATION_DIR = "C:\\dev\\temp";
	
	private final static Logger log = LoggerFactory.getLogger(ExtractIcons.class);
	
	public static void main(String[] args) throws IOException {
		new ExtractIcons(Paths.get(ECLIPSE_HOME), Paths.get(DESTINATION_DIR)).run();
	}
	
	private final ExtractResources extractResources;
	
	public ExtractIcons(Path sourceDir, Path destinationDir) {
		log.info("Icon extraction tool started; "
				+ "source: " + sourceDir + "; "
				+ "destination: " + destinationDir);
		extractResources = new ExtractResources(sourceDir, destinationDir, p -> {
			if (p.getFileName() == null) return false;
			String n = p.getFileName().toString();
			return (n.endsWith(".png") || n.endsWith(".gif") || n.endsWith(".svg"));
		});
	}
	
	public Map<String, ExtractedFile> run() {
		return extractResources.run();
	}
}
