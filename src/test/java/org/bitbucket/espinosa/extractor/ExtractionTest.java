package org.bitbucket.espinosa.extractor;

import static org.bitbucket.espinosa.extractor.ToStringNormalization.normalize;
import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.bitbucket.espinosa.extractor.run.ExtractIcons;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

public class ExtractionTest {
	
	/**
	 * For each test is creates temporary directory within system global
	 * temporary directory - in Windows OS it is something like:
	 * {@code C:\Users\Espinosa\AppData\Local\Temp\junit2454273810127757546\} -
	 * which is then deleted after every test.
	 */
	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();
	
	private Path testSourceDir = null;
	private URI testSourceUri = null; 
	private Path destinationDir = null;
	
	@Before
	public void before() throws Exception {
		testSourceUri = ClassLoader.getSystemClassLoader().getResource("").toURI();
		testSourceDir = new File(testSourceUri).toPath();
		destinationDir = tempFolder.getRoot().toPath();
	}
	
	@Test
	public void extract() throws IOException {		
		// action
		Map<String, ExtractedFile> r = new ExtractIcons(testSourceDir, destinationDir).run();
		
		// assert
		assertEquals("" +
				"arrow.png => IconRecord [name=arrow.png, locations=[archive1.zip!/pictures/arrow.png]]\n" + 
				"homefolder.png => IconRecord [name=homefolder.png, locations=[archive1.zip!/homefolder.png]]\n" + 
				"logo.png => IconRecord [name=logo.png, locations=[archive2.zip!/logo.png]]\n" + 
				"sample.1.gif => IconRecord [name=sample.1.gif, locations=[folder123/sample.gif]]\n" + 
				"sample.gif => IconRecord [name=sample.gif, locations=[archive2.zip!/sample.gif]]\n" + 
				"synced.1.png => IconRecord [name=synced.1.png, locations=[archive1.zip!/pictures/synced.png]]\n" + 
				"synced.2.png => IconRecord [name=synced.2.png, locations=[folder123/synced.png]]\n" + 
				"synced.png => IconRecord [name=synced.png, locations=[archive1.zip!/synced.png]]\n" + 
				"xterm.png => IconRecord [name=xterm.png, locations=[folder123/xterm.png]]", 
				normalize(r, toStringCustomization));
		assertEquals("" +
				"arrow.png\n" + 
				"arrow.png.txt\n" + 
				"homefolder.png\n" + 
				"homefolder.png.txt\n" + 
				"logo.png\n" + 
				"logo.png.txt\n" + 
				"sample.1.gif\n" + 
				"sample.1.gif.txt\n" + 
				"sample.gif\n" + 
				"sample.gif.txt\n" + 
				"synced.1.png\n" + 
				"synced.1.png.txt\n" + 
				"synced.2.png\n" + 
				"synced.2.png.txt\n" + 
				"synced.png\n" + 
				"synced.png.txt\n" + 
				"xterm.png\n" + 
				"xterm.png.txt", 
				normalize(listFilesFolder(destinationDir)));
	}
	
	private List<String> listFilesFolder(Path destinationDir) throws IOException {
		return Files.walk(destinationDir)
				.filter(Files::isRegularFile) // skip parent folder
				.map((f) -> {
			return f.getFileName().toString();
		}).collect(Collectors.toList());
	}
	
	
	ToStringNormalization.Customization toStringCustomization = new ToStringNormalization.Customization() {
		@Override
		public String customToString(Object o) {
			if (o == null) {
				return "null";
			} else if (o instanceof ExtractedFile) {
				return toString((ExtractedFile) o);
			} else {
				return o.toString();
			}
		}
		
		/**
		 * Relativize paths in {@link ExtractedFile} to make them comparable
		 * for any possible test.
		 */
		public String toString(ExtractedFile o) {
			List<String> normalizardLocations = o.getLocations().stream().map(p -> {
				String relPath = UriUtils.relativize(testSourceUri, p);
				return relPath;
			}).collect(Collectors.toList());
			return "IconRecord [name=" + o.getName() + ", locations=" + normalizardLocations + "]";
		}
	};
}
