package org.bitbucket.espinosa.extractor;

import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.stream.Collectors;

public class ToStringNormalization {

	/**
	 * Render any Collection to String, sorted and separated by newlines.
	 * 
	 * @param c
	 */
	public static String normalize(Collection<?> c) {
		return c.stream()
				.sorted()
				.map(item -> {
					return item.toString();
				}).collect(Collectors.joining("\n"));
	}

	/**
	 * Render any Map to String with focus on suitability for testing.
	 * 
	 * 1) sorted by key - predictable order necassary for map to map comparison.
	 * 
	 * 2) Customizable key and value to string transformation, providing options
	 * to prevent volatile parts.
	 * 
	 * @param m
	 *            map to be transformed to string
	 * @param cust
	 *            customization of key and value for every entry
	 */
	public static String normalize(Map<?, ?> m, Customization cust) {
		return m.entrySet()
				.stream()
				.sorted(ToStringNormalization::genericEntryComparing)
				.map(entry -> {
					StringBuffer sb = new StringBuffer();
					sb.append(cust.customToString(entry.getKey()));
					sb.append(" => ");
					sb.append(cust.customToString(entry.getValue()));
					return sb.toString();
				})
				.collect(Collectors.joining("\n"));
	}
	
	/**
	 * Render any Map to String with focus on suitability for testing.
	 * 
	 * 1) sorted by key - predictable order necassary for map to map comparison.
	 * 
	 * @param m
	 *            map to be transformed to string
	 */
	public static String normalize(Map<?, ?> m) {
		return normalize(m, useNaturalToString);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static int genericEntryComparing(Entry<?, ?> e1, Entry<?, ?> e2) {
		if (e1.getKey() instanceof Comparable && e2.getKey() instanceof Comparable) {
			return ((Comparable)e1.getKey()).compareTo(((Comparable)e2.getKey()));
		} else {
			return 0;
		}
	}

	public interface Customization {
		String customToString(Object o);
	}
	
	public static final Customization useNaturalToString = new Customization() {
		@Override
		public String customToString(Object o) {
			return Objects.toString(o);
		}	
	};
}
