package org.bitbucket.espinosa.extractor;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.bitbucket.espinosa.extractor.UniqueNameHandler;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 * Test {@link UniqueNameHandler}
 * 
 * @author Espinosa (6.4.2017)
 */
@RunWith(Parameterized.class)
public class UniqueNameHandlerTest {
	
	private final UniqueNameHandler h = new UniqueNameHandler();
	
	private final static Map<String, Object> emptyNamesMap = new HashMap<>();
	private final static Map<String, Object> namesMap1 = new HashMap<>();
	private final static Map<String, Object> namesMap2 = new HashMap<>();
	static {
		namesMap1.put("arrow.1.png", "");
		namesMap2.put("arrow.1.png", "");
		namesMap2.put("arrow.2.png", "");
	}
	
    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {     
                 { "arrow.png",     "arrow.1.png",   emptyNamesMap}, 
                 { "arrow.0.png",   "arrow.1.png",   emptyNamesMap},
                 { "arrow.1.png",   "arrow.2.png",   emptyNamesMap},
                 { "arrow.100.png", "arrow.101.png", emptyNamesMap},
                 { "arrow.png",     "arrow.2.png",   namesMap1},
                 { "arrow.png",     "arrow.3.png",   namesMap2}
           });
    }
    
    private String original; 
    private String transformedExpected;
    private Map<String, Object> namesMap;
    
	public UniqueNameHandlerTest(String original, String transformedExpected, Map<String, Object> namesMap) {
		this.original = original;
		this.transformedExpected = transformedExpected;
		this.namesMap = namesMap;
	}

	@Test 
	public void testWithEmptyMamesMap() {
		String transformed = h.increment(original, namesMap);
		assertEquals(transformedExpected, transformed);
	}
}
