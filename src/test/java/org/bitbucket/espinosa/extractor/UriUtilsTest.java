package org.bitbucket.espinosa.extractor;

import static org.junit.Assert.assertEquals;

import java.net.URI;

import org.junit.Test;

public class UriUtilsTest {

	@Test 
	public void testStripSchema() throws Exception {
		assertEquals(
			"C:/dev/workspace/z054-eclipse-icon-extractor/target/test-classes/",
			UriUtils.stripSchema(new URI("file:/C:/dev/workspace/z054-eclipse-icon-extractor/target/test-classes/")));		
		
		assertEquals(
			"C:/dev/workspace/proj123/target/test-classes/folder123/synced.png",
			UriUtils.stripSchema(new URI("file:///C:/dev/workspace/proj123/target/test-classes/folder123/synced.png")));

		assertEquals(
			"/home/espinosa/dev/workspace/proj123/target/test-classes/folder123/synced.png",
			UriUtils.stripSchema(new URI("file:///home/espinosa/dev/workspace/proj123/target/test-classes/folder123/synced.png")));

		assertEquals(
			"C:/dev/workspace/proj123/target/test-classes/archive2.zip!/logo.png",
			UriUtils.stripSchema(new URI("jar:file:///C:/dev/workspace/proj123/target/test-classes/archive2.zip!/logo.png")));
	}


	@Test 
	public void testRelativize() throws Exception {
		assertEquals(
			"archive2.zip!/logo.png",
			UriUtils.relativize(
				new URI("file:///C:/dev/workspace/proj123/target/test-classes/"), 
				new URI("jar:file:///C:/dev/workspace/proj123/target/test-classes/archive2.zip!/logo.png")));

		assertEquals(
			"folder123/synced.png",
			UriUtils.relativize(
				new URI("file:///C:/dev/workspace/proj123/target/test-classes/"),
				new URI("file:///C:/dev/workspace/proj123/target/test-classes/folder123/synced.png")));
	}
}
